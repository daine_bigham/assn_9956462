<?php 
    include('functions/functions.php');
    
    $id = $_GET['id'];
?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="css/main.css" type="text/css" 
        <meta char="UTF=8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Address Book</title>
        
    </head>
    <body>
        <section class="background"></section>
        <header>
            Address Book
        </header>
        <nav><ul class="navBar" id="nav"></ul></nav>
        <section class="container">

            <aside class="left" id="box1">
                <p class="names">All Names</p>
                <ul id="names" class="names">
                    <?php echo set_side_column(get_side_column()); ?>
                </ul>    
            </aside>
            <section class="content" id="box2">
                <?php 
                
                    if (!isset($id)) {
                        echo "No Name has been selected";
                    }
                    else {
                ?>        
                        <table>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Email</th>
                            </tr>
                <?php
                            echo showData(get_data($id), "index");
                ?>
                        </table>
                <?php                        
                    }
                ?>
            </section>
            
        <script src="js/scripts.js"></script>
        </section>
    </body>
</html>
        
  